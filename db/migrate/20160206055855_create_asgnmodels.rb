class CreateAsgnmodels < ActiveRecord::Migration
  def change
    create_table :asgnmodels do |t|
      t.string :name
      t.float :weight
      t.float :height
      t.string :color
      t.datetime :dateofbirth
      t.integer :numberoflanguagesspoken

      t.timestamps null: false
    end
  end
end

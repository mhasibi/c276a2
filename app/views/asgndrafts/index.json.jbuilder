json.array!(@asgndrafts) do |asgndraft|
  json.extract! asgndraft, :id, :name, :weight, :height, :color, :dateofbirth, :numberoflanguagesspoken
  json.url asgndraft_url(asgndraft, format: :json)
end

class AsgndraftsController < ApplicationController
  before_action :set_asgnmodels, only: [:show, :edit, :update, :destroy]

  # GET /asgndrafts
  # GET /asgndrafts.json
  def index
    @asgnmodels = Asgnmodels.all
  end

  # GET /asgndrafts/1
  # GET /asgndrafts/1.json
  def show
  end

  # GET /asgndrafts/new
  def new
    @asgnmodels = Asgnmodels.new
  end

  # GET /asgndrafts/1/edit
  def edit
  end

  # POST /asgndrafts
  # POST /asgndrafts.json
  def create
    @asgnmodels = Asgnmodels.new(asgndraft_params)

    respond_to do |format|
      if @asgnmodels.save
        format.html { redirect_to @asgnmodels, notice: 'Asgndraft was successfully created.' }
        format.json { render action: 'show', status: :created, location: @asgnmodels }
      else
        format.html { render action: 'new' }
        format.json { render json: @asgnmodels.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /asgndrafts/1
  # PATCH/PUT /asgndrafts/1.json
  def update
    respond_to do |format|
      if @asgndraft.update(asgndraft_params)
        format.html { redirect_to @asgndraft, notice: 'Asgndraft was successfully updated.' }
        format.json { render action: 'show', status: :ok, location: @asgndraft }
      else
        format.html { render action: 'edit' }
        format.json { render json: @asgndraft.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /asgndrafts/1
  # DELETE /asgndrafts/1.json
  def destroy
    @asgndraft.destroy
    respond_to do |format|
      format.html { redirect_to asgndrafts_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_asgndraft
      @asgndraft = Asgndraft.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def asgndraft_params
      params.require(:asgndraft).permit(:name, :weight, :height, :color, :dateofbirth, :numberoflanguagesspoken)
    end
end
